import discord
import asyncio
import random
from datetime import datetime


class BOFHBot(discord.Client):
    _logger = None
    _user = None
    _excuses = []

    def _setLogger(self, logger):
        self._logger = logger

    def _setExcuses(self, excuses):
        self._excuses = excuses

    @asyncio.coroutine
    async def on_ready(self):
        self._logger.info('Logged in as <{id}> "{username}"'.format(id=self.user.id, username=self.user.name))
        self._logger.info('---- Bot Ready')
        await self.change_presence(game=discord.Game(name='Office Despot'))

    @asyncio.coroutine
    async def on_message(self, message):
        content = message.clean_content

        self._logger.info('Message Received: ' + content)

        # dont reply to my own messages
        if message.author.id != self.user.id:

            # !excuse command
            try:
                if content.lower().index('!excuse') > -1:
                    self._logger.info('Excuse Requested!')
                    excuse = random.choice(self._excuses)
                    reply_message = '<@' + message.author.id + '>: ' + excuse
                    self._logger.info(
                        'Sending Excuse (' + excuse + ') to channel: ' + str(self.get_channel(message.channel.id)))
                    await self.send_message(self.get_channel(message.channel.id), reply_message)
            except:
                pass

            # "@me help" command
            try:
                if content.lower().index('@bofh bot help') > -1:
                    self._logger.info('Help Requested!')
                    await self.send_message(self.get_channel(message.channel.id),
                                            'Type "!excuse" anywhere in any message to get a convenient cop-out to ' +
                                            'satisfy a dimwitted customer or supervisor.')
            except:
                pass
