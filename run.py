import os
import sys
import logging
import atexit
import configparser
from util.bot import *

config = configparser.ConfigParser()
config.read('main.ini')

# Create console handler with a higher log level
logFormat = logging.Formatter(fmt='{asctime} - {name} - {levelname} - {message}', datefmt='%Y-%m-%d %H:%M:%S',
                              style="{")
handler = logging.StreamHandler(stream=sys.stdout)
handler.setFormatter(fmt=logFormat)

# Get all the logger channel objects
loggerBofhBot = logging.getLogger('bofhBot')
loggerDiscord = logging.getLogger('discord')

# Set various logging levels for each channel
loggerBofhBot.setLevel(level=logging.INFO)
loggerDiscord.setLevel(level=logging.WARNING)

# Attach all the handlers to each logger
loggerBofhBot.addHandler(handler)
loggerDiscord.addHandler(handler)

processPid = None
pidFile = ""
processManager = {}


# main program loop
def main():
    global processPid, pidFile, processManager

    # Get and Save Process ID
    processID = os.getpid()
    dir_path = os.path.dirname(os.path.realpath(__file__))
    pidFile = dir_path + "/bofhBot.pid"

    print(pidFile)

    # do not launch multiple copies of the process manager system
    if os.path.isfile(pidFile):
        print("BOFHBot Process ({}) already exists, exiting.".format(pidFile))
        sys.exit()

    # Write our processID to the PID file
    file_object = open(pidFile, 'w')
    file_object.write(str(processID))
    file_object.close()

    # Register a cleanup function to delete the PID file and shutdown all worker threads
    # when the master process shuts down
    atexit.register(_shutDown, pid=processID, pidFile=pidFile)

    # Startup Bot
    try:
        discord_logger = BOFHBot()
        discord_logger._setLogger(loggerBofhBot)
        discord_logger._setExcuses(excuses=config.get('excuses', 'lines').split('\n'))
        discord_logger.run(config.get('bot', 'token'))
    except Exception as e:
        loggerBofhBot.error('!!! Discord WebSocket Connection Severed: {content}'.format(content=e))
        raise

    return 0


def _shutDown(pid, pidFile):
    if os.path.isfile(pidFile):
        os.unlink(pidFile)


# do it bby
if __name__ == "__main__":
    main()
